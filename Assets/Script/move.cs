using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{

    private Rigidbody2D rb;

    private SpriteRenderer SR;
    private bool caer;
    Vector2 input;
    public float speed;

    public float FuerzaSaltito;

    public bool suelo;

    public Animator animatorpersonaje;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        SR = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.D))
        {
            SR.flipX = false;

        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            SR.flipX = true;
        }


        

        input.x = Input.GetAxis("Horizontal");


        if (input.x != 0) 
        {
            animatorpersonaje.SetBool("Correr", true);
            animatorpersonaje.SetBool("Respirar", false);
        }
        else
        {
            animatorpersonaje.SetBool("Respirar", true);
            animatorpersonaje.SetBool("Correr", false);
        }



        animatorpersonaje.SetBool("Salto", false);
        animatorpersonaje.SetBool("Caida", false);
       

        if (rb.velocity.y > 0.001f)
        {
            animatorpersonaje.SetBool("Salto", true);
            
        }
        if (rb.velocity.y < -0.001f)

        {
            animatorpersonaje.SetBool("Caida", true);
        }


        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1000, LayerMask.GetMask("Sueloo"));

        if ((hit.collider != null) && (hit.collider.CompareTag("Sueloo")))
            {
            suelo = true;
        }
       else
        {
            suelo = false;
        }


        



        if (suelo = true && (Input.GetKeyDown(KeyCode.Space)))
            {

            suelito();
        }
    }
    private void FixedUpdate()
    {
        

        rb.velocity = new Vector2(input.x * speed * Time.fixedDeltaTime, rb.velocity.y);
    }

    void suelito()
    {
       
        rb.velocity = new Vector2(rb.velocity.x, FuerzaSaltito);
        suelo = false;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Destroy(gameObject);
            Gamemanager.muerto = true;

        }

    }
}
