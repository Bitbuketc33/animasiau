using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Gamemanager : MonoBehaviour
{

    public static int monedass;

    public Text moneText;

    public static bool muerto;

    public GameObject gameover;

    // Start is called before the first frame update
    void Start()
    {
        gameover.SetActive(false);
        muerto = false;
    }

    // Update is called once per frame
    void Update()
    {
        moneText.text = ("Monedas") + monedass.ToString();
        if (muerto == true)
        {
            muerte();
        }

    }

    public void muerte()
    {
        gameover.SetActive(true);
    }

    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}
